import {white, shadow, secondarytext, primary} from './color';
import font from './fonts';
import {sm, bm} from './bordersize';

export default {
  maincontainer: {
    flex: 1,
  },

  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 30,
  },

  plusbutton: {
    position: 'absolute',
    right: 10,
    bottom: 10,
  },

  activitycontainer: {
    //  backgroundColor: white,
    marginTop: 25,
    //borderRadius: sm,
    //padding: 10,
    //marginRight: 10,
    //marginTop: 17,
  },

  eventcontainer: {
    margin: 20,
    borderRadius: bm,
    backgroundColor: white,
    padding: 20,
    shadowColor: shadow,
    shadowOffset: {width: 0, height: 3},
    shadowOpacity: 1,
    shadowRadius: 5,
    elevation: 3,
  },

  eventtile: {
    fontSize: font.h1,
    alignSelf: 'center',
  },

  eventtext: {
    color: secondarytext,
    marginVertical: 10,
  },

  namebodycontainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 0,
    marginVertical: 5,
    backgroundColor: white,
    shadowColor: shadow,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 1,
    shadowRadius: 3,
    elevation: 1,
  },

  nametextinput: {
    height: 40,
    padding: 0,
    flex: 1,
    paddingLeft: 10,
  },

  buttoncontainer: {
    paddingHorizontal: 30,
    marginTop: 20,
    marginBottom: 5,
  },

  linearGradient: {
    borderRadius: 40,
    backgroundColor: primary,
    borderRadius: 12,
  },

  buttontextStyle: {
    alignSelf: 'center',
    color: white,
    fontSize: 17,
    fontWeight: '500',
    paddingTop: 10,
    paddingBottom: 10,
  },

  headericon: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
    marginHorizontal: 10,
  },

  txt: {
    fontSize: font.h1,
    color: white,
  },

  middle: {
    width: 50,
  },

  textcontainer: {
    alignItems: 'center',
    flex: 1,
  },

  headerimage: {
    width: 50,
    paddingHorizontal: 10,
  },

  headercontainer: {
    alignItems: 'center',
    flexDirection: 'row',
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: primary,
    backgroundColor: primary,
  },
  headermain: {
    width: 50,
    flexDirection: 'row',
  },
};
