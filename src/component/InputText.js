import React from 'react';
import {Text} from 'react-native';
import style from '../utils/style';

const InputText = props => {
  return (
    <Text style={[style.eventtile, props.style]} onPress={props.onPress}>
      {props.children}
    </Text>
  );
};

export {InputText};
