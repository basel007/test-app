import React, {useState} from 'react';
import {View, TextInput} from 'react-native';
import style from '../utils/style';

const TextInputs = props => {
  const [iconName, setStateIcon] = useState(props.inActiveIcon);
  const [underLineColor] = useState('#333');

  const onFocus = () => {
    setStateIcon({
      iconName: props.activeIcon,
      underLineColor: '#333',
    });
  };
  const onBlur = () => {
    setStateIcon({
      iconName: props.inActiveIcon,
      underLineColor: '#333',
    });
  };

  return (
    <View style={[props.style]}>
      <TextInput
        {...props}
        style={[style.nametextinput]}
        onFocus={onFocus}
        onBlur={onBlur}
      />
    </View>
  );
};
export default TextInputs;
