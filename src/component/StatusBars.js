import React from 'react';
import {View, StatusBar} from 'react-native';
import {primary} from '../utils/color';

export default function StatusBars(props) {
  return (
    <View>
      <StatusBar barStyle="light-content" backgroundColor={primary} />
    </View>
  );
}
