import React from 'react';
import {Text} from 'react-native';
import style from '../utils/style';

const TitleText = props => {
  return (
    <Text style={[style.eventtext, props.style]} onPress={props.onPress}>
      {props.children}
    </Text>
  );
};

export {TitleText};
