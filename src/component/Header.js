import React from 'react';
import {View, Text, TouchableOpacity, SafeAreaView} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import style from '../utils/style';
import {white} from '../utils/color';
export default function Header(props) {
  return (
    <SafeAreaView style={{}}>
      <View style={style.headercontainer}>
        <View style={style.headermain}>
          {props.isBack ? (
            <TouchableOpacity
              onPress={() => props.navigation.goBack()}
              style={style.headerimage}>
              <Ionicons name="ios-arrow-round-back" size={32} color={white} />
            </TouchableOpacity>
          ) : null}
        </View>
        <View style={style.textcontainer}>
          <Text style={style.txt}>{props.name}</Text>
        </View>
        <View style={style.middle} />
      </View>
    </SafeAreaView>
  );
}
