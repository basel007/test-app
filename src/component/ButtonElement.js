import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import style from '../utils/style';
const ButtonElement = props => {
  return (
    <View>
      <View style={[style.linearGradient, props.style]}>
        <TouchableOpacity onPress={props.onPress}>
          <Text style={[style.buttontextStyle, props.buttonStyle]}>
            {props.children}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export {ButtonElement};
