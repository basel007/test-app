import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import CalenderScreen from './screen/CalenderScreen';
import AddEventScreen from './screen/AddEventScreen';
import {createStackNavigator} from 'react-navigation-stack';
const EventStack = createStackNavigator(
  {
    CalenderScreen: CalenderScreen,
    AddEventScreen: AddEventScreen,
  },
  {
    initialRouteName: 'CalenderScreen',
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    },
  },
);

const AppSwitchNavigator = createSwitchNavigator({
  EventStack: {screen: EventStack},
});

export default createAppContainer(AppSwitchNavigator);
