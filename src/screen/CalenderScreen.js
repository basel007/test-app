import React, {useState, useEffect} from 'react';
import {Text, View, TouchableOpacity, AsyncStorage} from 'react-native';
import {Agenda} from 'react-native-calendars';
import AntDesign from 'react-native-vector-icons/AntDesign';
import moment from 'moment';
import style from '../utils/style';
import {silver} from '../utils/color';
import {useDispatch, useSelector} from 'react-redux';
import {getevent} from '../redux/actions';

export default function CalenderScreen(props) {
  const [items, setitems] = useState({});
  const [agenda, setAgenda] = useState([]);
  const dispatch = useDispatch();

  const eventReducer = useSelector(state => state.eventReducer);
  const {events} = eventReducer;
  useEffect(() => getData(), []);

  const getData = () => {
    AsyncStorage.getItem('events', (err, events) => {
      events = JSON.parse(events);
      if (err) alert(err.message);
      else if (events !== null) {
        let itemsObj = {};
        events.map(record => {
          let dateKey = moment(record.date).format('YYYY-MM-DD');

          if (!itemsObj.hasOwnProperty(dateKey)) {
            itemsObj[dateKey] = [];
          }
          itemsObj[dateKey].push({
            name: record.name,
          });
        });
        dispatch(getevent(events));
        setitems(itemsObj);
        setAgenda(agenda);
      }
    });
  };

  const loadItems = day => {
    setTimeout(() => {
      for (let i = -15; i < 85; i++) {
        const time = day.timestamp + i * 24 * 60 * 60 * 1000;
        const strTime = timeToString(time);
        if (!items[strTime]) {
          items[strTime] = [];
          const numItems = Math.floor(Math.random() * 5);
          for (let j = 0; j < numItems; j++) {
            items[strTime].push({
              name: '',
              height: Math.max(50, Math.floor(Math.random() * 150)),
            });
          }
        }
      }
      const eventsValue = {};
      Object.keys(items).forEach(key => {
        eventsValue[key] = items[key];
      });
      setitems(eventsValue);
    }, 1000);
  };

  const renderItem = item => {
    return (
      <TouchableOpacity style={style.emptyDate}>
        <Text>{item && item.hasOwnProperty('name') ? item.name : null}</Text>
      </TouchableOpacity>
    );
  };

  const rowHasChanged = (r1, r2) => {
    return r1.name !== r2.name;
  };

  const renderEmptyDate = () => {
    return (
      <View style={style.emptyDate}>
        <Text>No Event Available </Text>
      </View>
    );
  };

  const timeToString = time => {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  };
  return (
    <View style={style.maincontainer}>
      <Agenda
        items={items}
        loadItemsForMonth={e => loadItems(e)}
        selected={'2020-03-29'}
        renderItem={e => renderItem(e)}
        rowHasChanged={(v, x) => rowHasChanged(v, x)}
        renderEmptyDate={e => renderEmptyDate(e)}
      />
      <TouchableOpacity
        onPress={() => props.navigation.navigate('AddEventScreen')}
        style={style.plusbutton}>
        <AntDesign name="pluscircle" size={40} color={silver} />
      </TouchableOpacity>
    </View>
  );
}
