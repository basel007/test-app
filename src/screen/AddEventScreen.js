import React, {useState} from 'react';
import {View, Text, AsyncStorage} from 'react-native';
import StatusBars from '../component/StatusBars';
import {useDispatch} from 'react-redux';
import {addevent} from '../redux/actions';
import DatePicker from 'react-native-datepicker';
import Toast from 'react-native-simple-toast';
import Moment from 'moment';
import style from '../utils/style';
import {InputText} from '../component/InputText';
import {ButtonElement} from '../component/ButtonElement';
import Header from '../component/Header';
import TextInputs from '../component/TextInputs';
import {TitleText} from '../component/TitleText';

export default function AddEventScreen(props) {
  const dispatch = useDispatch();
  const [name, setFName] = useState('');
  const [error] = useState('');
  const [date, setDate] = useState(new Date());
  const [errors, setError] = useState({});

  const [validationerror, setValidation] = useState(false);

  const validateForm = () => {
    let formValid = true;
    let errors = {};
    if (date === '') {
      formValid = false;
      errors['date'] = '*Date is required';
    } else {
      delete errors['date'];
    }
    if (name === '') {
      formValid = false;
      errors['name'] = '*Event Name  is required';
    } else {
      delete errors['name'];
    }
    setError(errors);
    setValidation(true);
    return formValid;
  };

  const generateID = () => {
    let d = new Date().getTime();
    let id = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(
      c,
    ) {
      let r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c == 'x' ? r : (r & 0x3) | 0x8).toString(5);
    });

    return id;
  };

  const onSubmithandle = () => {
    try {
      if (validateForm()) {
        Moment.locale('en');
        let event_ = {};

        let id = generateID();
        event_ = {id: id, name: name, date: Moment(date).format()};
        const events = event_;
        var myArray = [events];
        AsyncStorage.setItem('events', JSON.stringify(myArray), () => {
          dispatch(addevent(event_));
          Toast.show('Event Successfully Added');
        });
        props.navigation.goBack();
      }
    } catch (error) {
      console.log('Error occuring data');
    }
  };
  return (
    <View>
      <StatusBars />
      <Header name={'Event'} {...props} isBack={true} />
      <View style={style.eventcontainer}>
        <View>
          <InputText>Event Details</InputText>
          <>
            <TitleText>Name</TitleText>
            <View style={style.namebodycontainer}>
              <TextInputs
                onChangeText={text => setFName(text)}
                value={name}
                style={style.nametextinput}
                placeholder={'Event Name'}
              />
            </View>
            {validationerror != false && (
              <Text style={{marginLeft: 5}}>{errors['name']}</Text>
            )}
          </>
          <>
            <TitleText>Date</TitleText>
            <View style={style.namebodycontainer}>
              <DatePicker
                style={{width: 200}}
                date={date}
                mode="date"
                placeholder="select date"
                format="YYYY-MM-DD"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 5,
                    top: 0,
                    marginLeft: 0,
                    marginTop: 5,
                  },
                  dateInput: {
                    borderWidth: 0,
                  },
                }}
                onDateChange={date => setDate(date)}
              />
            </View>
            {validationerror != false && (
              <Text style={{marginLeft: 5}}>{errors['date']}</Text>
            )}
          </>
          <>
            <View style={style.buttoncontainer}>
              <ButtonElement onPress={e => onSubmithandle(e)}>
                Submit
              </ButtonElement>
            </View>
          </>
        </View>
      </View>
    </View>
  );
}
