import {combineReducers} from 'redux';

import {EVENT_AVAILABLE, ADD_EVENT} from './actions';

let dataState = {events: []};

const eventReducer = (state = dataState, action) => {
  switch (action.type) {
    case ADD_EVENT:
      let {event} = action.data;
      let clone = JSON.parse(JSON.stringify(state.events));

      return {...state, events};

    case EVENT_AVAILABLE:
      let {events} = action.data;

      return {...state, events};

    default:
      return state;
  }
};

const rootReducer = combineReducers({eventReducer});

export default rootReducer;
