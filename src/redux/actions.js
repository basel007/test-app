export const EVENT_AVAILABLE = 'EVENT_AVAILABLE';
export const ADD_EVENT = 'ADD_EVENT';

export const getevent = events => ({
  type: EVENT_AVAILABLE,
  data: {events},
});
export const addevent = event => ({
  type: ADD_EVENT,
  data: {event},
});
