import React from 'react';
import Route from './src/Route';
import {Provider} from 'react-redux';
import store from './src/redux/store';
const App: () => React$Node = () => {
  console.disableYellowBox = true;
  return (
    <Provider store={store}>
      <Route />
    </Provider>
  );
};
export default App;
